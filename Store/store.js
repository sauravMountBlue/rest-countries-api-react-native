import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from '../Reducers';

const intitalState = {};

const middleware = [thunk];

const store = createStore(rootReducer, intitalState, applyMiddleware(...middleware));

export default store;
