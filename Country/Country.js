import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Button, withTheme } from 'react-native-paper';
import SVGImage from '../SVGRenderer';

const handleBackPress = ({ setDetailView }) => {
	setDetailView(false);
};

const getBorderCountriesData = ({ borders, countriesData }) => {
	return countriesData.filter(ele => borders.includes(ele.alpha3Code));
};

const handleBorderPress = ({ data, filterCountryData }) => {
	filterCountryData(data);
};

const Country = props => {
	let {
		setDetailView,
		theme: { lightTheme, darkTheme },
		themeType,
		data: {
			flag,
			name,
			nativeName,
			population,
			region,
			subregion,
			capital,
			topLevelDomain,
			currencies,
			languages,
			borders,
		},
		countriesData,
		filterCountryData,
	} = props;
	topLevelDomain = topLevelDomain.join(', ');
	currencies = currencies.map(ele => ele.name).join(', ');
	languages = languages.map(ele => ele.name).join(', ');

	const borderData = getBorderCountriesData({ borders, countriesData });

	const { colors } = themeType === 'dark' ? darkTheme : lightTheme;
	return (
		<View style={[styles.detailContainer, { backgroundColor: colors.darkBackground }]}>
			<Button
				icon="arrow-left"
				style={[styles.buttonContainer, { backgroundColor: colors.lightBackground }]}
				onPress={() => handleBackPress({ setDetailView })}
				color={colors.fontColor}
			>{`Back`}</Button>
			<View style={styles.imageContainer}>
				<SVGImage style={{ width: `100%`, height: 200 }} source={{ uri: flag }} />
			</View>
			<View style={styles.detailsContainer}>
				<Text style={[styles.nameText, { color: colors.fontColor }]}>{name}</Text>
				<View style={styles.section1}>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Native Name:`}</Text>
						<Text style={{ color: colors.fontColor }}>{nativeName}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Population:`}</Text>
						<Text style={{ color: colors.fontColor }}>{population}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Region:`}</Text>
						<Text style={{ color: colors.fontColor }}>{region}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Sub Region:`}</Text>
						<Text style={{ color: colors.fontColor }}>{subregion}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Capital`}</Text>
						<Text style={{ color: colors.fontColor }}>{capital}</Text>
					</View>
				</View>
				<View style={styles.section2}>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Top Level Domain:`}</Text>
						<Text style={{ color: colors.fontColor }}>{topLevelDomain}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Currencies:`}</Text>
						<Text style={{ color: colors.fontColor }}>{currencies}</Text>
					</View>
					<View style={styles.flexRow}>
						<Text style={[{ color: colors.fontColor }, styles.boldAndSpace]}>{`Languages:`}</Text>
						<Text style={{ color: colors.fontColor }}>{languages}</Text>
					</View>
				</View>
				<View style={styles.section3}>
					<Text style={[{ color: colors.fontColor }, styles.borderText]}>{`Border Countries:`}</Text>
					<View style={styles.borders}>
						{borderData.map(ele => (
							<Button
								key={ele.alpha3Code}
								style={[styles.borderCountriesButton, { backgroundColor: colors.lightBackground }]}
								onPress={() => handleBorderPress({ data: ele, filterCountryData })}
								color={colors.fontColor}
							>
								{ele.name}
							</Button>
						))}
					</View>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	detailContainer: {
		marginTop: 30,
	},
	buttonContainer: {
		width: 120,
		marginLeft: 24,
		marginBottom: 30,
		marginTop: 30,
		elevation: 5,
	},
	imageContainer: {
		width: `100%`,
		paddingLeft: 24,
		paddingRight: 24,
		marginBottom: 30,
	},
	imageStyle: {
		height: 200,
		width: '100%',
	},
	flexRow: {
		flexDirection: 'row',
	},
	detailsContainer: {
		marginLeft: 24,
		marginRight: 24,
	},
	section1: {
		marginBottom: 30,
	},
	section2: {
		marginBottom: 30,
	},
	section3: {
		marginBottom: 30,
	},
	borders: {
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	borderCountriesButton: {
		width: 140,
		marginBottom: 12,
		marginRight: 12,
		elevation: 5,
	},
	borderText: {
		fontSize: 20,
		fontWeight: 'bold',
		marginBottom: 16,
	},
	nameText: {
		fontSize: 24,
		fontWeight: 'bold',
		marginBottom: 30,
	},
	boldAndSpace: {
		fontWeight: 'bold',
		marginRight: 8,
	},
});

export default withTheme(Country);
