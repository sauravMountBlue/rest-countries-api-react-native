import React from 'react';
import { Appbar, Button, Text, withTheme } from 'react-native-paper';
import { StyleSheet } from 'react-native';

const handleThemeSwitcherPress = ({ themeType, setTheme }) => {
	setTheme(themeType === 'dark' ? 'light' : 'dark');
};

const Header = props => {
	const {
		theme: { lightTheme, darkTheme },
		themeType,
		setTheme,
	} = props;

	const { colors } = themeType === 'dark' ? darkTheme : lightTheme;
	const headerText = themeType === 'dark' ? `Light Mode` : `Dark Mode`;
	const moonIcon =
		themeType === 'dark' ? require('../../../assests/moonWhite.png') : require('../../../assests/moonBlack.png');

	return (
		<Appbar style={[styles.top, { backgroundColor: colors.lightBackground }]}>
			<Text style={[{ color: colors.fontColor }, styles.headerText]}>{`Where in the world?`}</Text>
			<Button
				style={{ backgroundColor: colors.lightBackground, elevation: 5 }}
				icon={moonIcon}
				color={colors.fontColor}
				onPress={() => handleThemeSwitcherPress({ themeType, setTheme })}
			>
				{headerText}
			</Button>
		</Appbar>
	);
};

const styles = StyleSheet.create({
	top: {
		height: 60,
		justifyContent: 'space-between',
		paddingLeft: 12,
		paddingRight: 12,
	},
	headerText: {
		fontSize: 20,
		fontWeight: `bold`,
	},
});

export default withTheme(Header);
