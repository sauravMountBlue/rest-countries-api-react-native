import { actionTypes } from '../Actions';

const { FETCH_COUNTRIES } = actionTypes;

const initalState = {
	data: [],
	searchData: [],
};

const countryReducer = (state = initalState, action) => {
	switch (action.type) {
		case FETCH_COUNTRIES:
			return {
				...state,
				data: action.data,
				searchData: action.searchData,
			};
		default:
			return state;
	}
};

export default countryReducer;
