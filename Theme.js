import { DefaultTheme } from 'react-native-paper';
import { colors } from './static';

const { darkBlue, veryDarkBlueBG, white, veryLightGrey, veryDarkBlue, darkGrey } = colors;
const Theme = {
	...DefaultTheme,
	lightTheme: {
		colors: {
			...DefaultTheme.colors,
			lightBackground: white,
			darkBackground: veryLightGrey,
			fontColor: veryDarkBlue,
			borderColor: darkGrey,
		},
	},
	darkTheme: {
		colors: {
			...DefaultTheme.colors,
			lightBackground: darkBlue,
			darkBackground: veryDarkBlueBG,
			fontColor: white,
			borderColor: veryDarkBlue,
		},
	},
	fonts: {
		...DefaultTheme.fonts,
		bold: {
			fontFamily: 'Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif',
			fontWeight: '800',
		},
	},
};

export default Theme;
