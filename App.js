import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as ReduxProvider } from 'react-redux';
import Theme from './Theme';
import Countries from './Countries';
import { store } from './Store';

const App = () => {
	return (
		<ReduxProvider store={store}>
			<PaperProvider theme={Theme}>
				<ScrollView style={styles.container}>
					<Countries />
				</ScrollView>
			</PaperProvider>
		</ReduxProvider>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 40,
		paddingBottom: 40,
	},
});

export default App;
